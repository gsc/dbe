/*
 * NAME
 *     dbe - expand double-braced references with the values from environment
 *
 * SYNOPSIS
 *     dbe [-kn] [-i SUFFIX] FILE [FILE...]
 *     dbe -h
 *
 * DESCRIPTION
 *     For each FILE from the command line, expands each occurrence of {{X}}
 *     in the file with the value of the environment variable X.  By default,
 *     expansion goes to the standard error.  The -i option requests
 *     expansion in place.  In this case a backup copy of each FILE is
 *     created with the name constructed by concatenating the file name and
 *     SUFFIX.  If SUFFIX is empty string, no backups are created.
 *
 *     It is an error if the variable X is not defined.  In this case, the
 *     program prints a diagnostic message on the standard error with the
 *     exact location (file name and line) of the error and name of the
 *     variable in question.  The default behavior is to exit immediately
 *     at the first such error.  It is changed by the -k ("keep going")
 *     option, which instructs the program to continue, leaving the
 *     reference to the undefined variable untouched.  The program 
 *     exits with the status code 65 if undefined variables have been
 *     encountered.
 *
 * OPTIONS
 *     -i SUFFIX
 *         Edit files in place, saving backups with the specified
 *         SUFFIX, unless it is an empty string, in which case no backups
 *         are created.
 *
 *     -k  Keep going after encountering undefined variables.  Leave
 *         undefined references untouched.
 *
 *     -n  Dry-run mode.  Process all files reporting each occurrence of
 *         undefined variable, but producing no output.  Implies -k.
 *
 *     -h  Print a short usage summary.
 *
 * EXIT CODES
 *     0   Success.
 *     64  Command line usage error.
 *     65  One or more variables referred to in the files are not defined.
 *     66  Input file cannot be opened.
 *     71  Operating system error (access to the file denied, etc.)
 *     73  Unable to create output file.
 *
 * AUTHOR
 *     Sergey Poznyakoff
 *
 * LICENSE
 *     THE BEER-WARE LICENSE" (Revision 42):
 *     <gray@gnu.org> wrote this file.  As long as you retain this notice you
 *     can do whatever you want with this stuff. If we meet some day, and you
 *     think this stuff is worth it, you can buy me a beer in return.
 */  
%{
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sysexits.h>
	
char const *progname;
char const *filename;
size_t filename_len;
unsigned lineno;
int keep_going;
char *backup_suffix;
size_t backup_suffix_size;
int to_stdout = 1;

int status = 0;
%}
%option noinput
%%
\n  {
    lineno++;
    ECHO;
}
\{\{[A-Za-z_][A-Za-z0-9_]*\}\} {
	char const *name = yytext + 2;
	char const *val;
    
	yytext[yyleng-2] = 0;
	val = getenv(name);
	if (val) {
		size_t len = strlen(val);
		while (len > 0) {
			--len;
			unput(val[len]);
		}
	} else {
		fprintf(stderr, "%s:%u: variable %s not defined\n",
			filename, lineno, name);
		yytext[yyleng-2] = '}';
		ECHO;
		status = EX_DATAERR;
		if (!keep_going)
			exit(status);
	}
  }
%%
static char **input_vector;
static int input_index;
static char *outfile_name;
static size_t outfile_name_size;

static int
open_input(char const *name)
{
	FILE *fp;

	if ((fp = fopen(name, "r")) == NULL) {
		fprintf(stderr, "%s: can't open %s\n", progname, name);
		exit(errno == ENOENT ? EX_NOINPUT : EX_OSERR);
	}
	yyin = fp;
	filename = name;
	filename_len = strlen(name);
	lineno = 1;
	return 0;
}

void *
buf2nrealloc(void *p, size_t *pn, size_t s)
{
        size_t n = *pn;
        char *newp;
	
        if (!p) {
                if (!n) {
                        /* The approximate size to use for initial small
                           allocation requests, when the invoking code
                           specifies an old size of zero.  64 bytes is
                           the largest "small" request for the
                           GNU C library malloc.  */
                        enum { DEFAULT_MXFAST = 64 };

                        n = DEFAULT_MXFAST / s;
                        n += !n;
                }
        } else {
                /* Set N = ceil (1.5 * N) so that progress is made if N == 1.
                   Check for overflow, so that N * S stays in size_t range.
                   The check is slightly conservative, but an exact check isn't
                   worth the trouble.  */
                if ((size_t) -1 / 3 * 2 / s <= n) {
                        errno = ENOMEM;
                        return NULL;
                }
                n += (n + 1) / 2;
        }

        newp = realloc(p, n * s);
        if (!newp)
                return NULL;
        *pn = n;
        return newp;
}

void
filename_realloc(char **pnamebuf, size_t *bufsize, size_t reqsize)
{
	char *name = *pnamebuf;
	size_t size = *bufsize;
	
	while (size < reqsize) {
		if ((name = buf2nrealloc(name, &size, 1)) == NULL) {
			perror("buf2nrealloc");
			exit(EX_OSERR);
		}
	}
	*pnamebuf = name;
	*bufsize = size;
}

static int
create_temp_output(void)
{
	int fd;
	size_t len = filename_len;
	static char pfile[] = "dbeXXXXXX";
	static size_t plen = sizeof(pfile);
	
	while (len > 0 && filename[len-1] != '/')
		len--;

  	filename_realloc(&outfile_name, &outfile_name_size, len + plen);
	memcpy(outfile_name, filename, len);
	memcpy(outfile_name + len, pfile, plen);

	fd = mkstemp(outfile_name);
	if (fd == -1) {
		fprintf(stderr, "%s: can't create output file for %s: %s\n",
			progname, filename, strerror(errno));
		return EX_CANTCREAT;
	}
	yyout = fdopen(fd, "w");
	if (!yyout) {
		fprintf(stderr, "%s: fdopen failed: %s\n",
			progname, strerror(errno));	
		close(fd);
		return EX_OSERR;
	}
	return 0;
}

static char *backup_file_name;
static size_t backup_file_name_size;

static void
create_backup_file(void)
{
	if (access(filename, F_OK)) {
		if (errno != ENOENT) {
			fprintf(stderr, "%s: can't access %s: %s",
				progname, filename, strerror(errno));
			exit(EX_OSERR);
		}
		return;
	}

	if (backup_suffix_size == 1) {
		if (unlink(filename)) {
			fprintf(stderr, "%s: can't unlink %s: %s",
				progname, filename, strerror(errno));
			exit(EX_OSERR);
		}
	} else {
		filename_realloc(&backup_file_name, &backup_file_name_size,
				 filename_len + backup_suffix_size);
		memcpy(backup_file_name, filename, filename_len);
		memcpy(backup_file_name + filename_len, backup_suffix,
		       backup_suffix_size);
		if (unlink(backup_file_name) && errno != ENOENT) {
			fprintf(stderr, "%s: can't unlink existing backup file %s: %s\n",
				progname, backup_file_name, strerror(errno));
			exit(EX_OSERR);
		}
		
		if (rename(filename, backup_file_name)) {
			fprintf(stderr, "%s: can't rename %s to %s: %s\n",
				progname, filename, backup_file_name, strerror(errno));
			exit(EX_CANTCREAT);
		}
	}
}

static int
flush_temp_output(void)
{
	fflush(yyout);
	if (rename(outfile_name, filename)) {
		fprintf(stderr, "%s: can't rename %s to %s: %s\n",
			progname, outfile_name, filename, strerror(errno));
		exit(EX_CANTCREAT);
	}
	fclose(yyout);
	return 0;
}

int
yywrap(void)
{
	fclose(yyin);
	if (!to_stdout) {
		create_backup_file();
		flush_temp_output();
	}
	if (input_vector[input_index] &&
	    open_input(input_vector[input_index++]) == 0) {
		if (!to_stdout) {
			int rc;
			if ((rc = create_temp_output()) != 0) {
				status = rc;
				return 1;
			}
		}
		return 0;
	}
	return 1;
}

void
usage(FILE *fp)
{
	fprintf(fp, "usage: %s [-k] [-i SUF] FILE [FILE...]\n", progname);
	fprintf(fp, "expands each occurrence of {{X}} in FILEs with the value of the\n"
		"environment variable X.\n");
	fprintf(fp, "\nOptions are:\n\n");
	fprintf(fp, "  -k      continue if undefined variable is encountered\n");
	fprintf(fp, "  -i SUF  replace variables in place, saving backups with the specified suffix;\n");
	fprintf(fp, "          a zero-length SUF means no backup will be created;\n");
	fprintf(fp, "  -n      do nothing, only report undefined variables (implies -k)\n");
	fprintf(fp, "  -h      print a short help summary and exit\n");
	fprintf(fp, "\nIn the absence of -i option, expanded material goes to standard output.\n");
}

int
main(int argc, char **argv)
{
	int c;
	int dry_run = 0;
	
	progname = argv[0];
	while ((c = getopt(argc, argv, "ki:hn")) != EOF) {
		switch (c) {
		case 'k':
			keep_going = 1;
			break;
			
		case 'i':
			to_stdout = 0;
			backup_suffix = optarg;
			backup_suffix_size = strlen(backup_suffix) + 1;
			break;

		case 'h':
			usage(stdout);
			return 0;

		case 'n':
			dry_run = 1;
			break;
			
		default:
			usage(stderr);
			return EX_USAGE;
		}
	}

	if (dry_run) {
		int fd;
		
		if (to_stdout == 0) {
			fprintf(stderr,
				"%s: options -t and -i cannot be used together\n",
				progname);
			return EX_USAGE;
		}
		if ((fd = open("/dev/null", O_WRONLY)) == -1 ||
		    dup2(fd, 1) == -1) {
			fprintf(stderr, "%s: can't open /dev/null for writing: %s\n",
				progname, strerror(errno));
			return EX_OSERR;
		}
		close(fd);
		keep_going = 1;
	}
		
	input_vector = argv + optind;
	input_index = 0;
	if (input_vector[input_index] == NULL) {
		fprintf(stderr, "%s: no input files\n", progname);
		usage(stderr);
		return EX_USAGE;
	}

	open_input(input_vector[input_index++]);
	if (!to_stdout && (status = create_temp_output()) != 0)
		return status;
	while (yylex())
		;
	return status;
}
    
    
