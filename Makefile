CPPFLAGS =
CFLAGS = -O2 -Wall
LEXFLAGS = 

PACKAGE = dbe
VERSION = 0.1

dbe: lex.yy.c
	$(CC) -odbe $(CPPFLAGS) $(CFLAGS) lex.yy.c
lex.yy.c: dbe.l
	flex $(LEXFLAGS) dbe.l
clean:
	rm -f dbe lex.yy.c
DISTNAME = $(PACKAGE)-$(VERSION)
DISTDIR = $(DISTNAME)
distdir: lex.yy.c
	rm -rf $(DISTDIR)
	mkdir $(DISTDIR)
	cp dbe.l Makefile lex.yy.c $(DISTDIR)
dist: distdir
	tar cf $(DISTNAME).tar $(DISTDIR)
	rm -rf $(DISTDIR)
